#ifndef POWER_MONITOR_H
#define POWER_MONITOR_H
#include<stdint.h>
int32_t power_monitor_init(void);
int32_t read_voltage(float*);
int32_t read_current(float*);
#endif