#include <assert.h>
#include <power_monitor.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

int main (void)
{
    assert(power_monitor_init()==0);
    float voltage, current;
    assert(read_voltage(&voltage)==0);
    printf("Voltage %f V\n", voltage);
    assert(read_current(&current)==0);
    printf("Current %f A\n", current);
    return 0;
}