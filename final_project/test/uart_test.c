/**
 * Many thanks to Pieter Jan for parts of this code
 */

#include <assert.h>

#include <bcm_host.h>

#include <fcntl.h>
#include <sched.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <termios.h>

#include <unistd.h>

#include <wiringPi.h>
/**
 * Sets up the scheduler so that it is running in
 * real-time mode, and so that this process has the highest
 * priority possible to reduce the chances that it is
 * interrupted by the kernel.
 */
void configure_scheduler(void)
{
    int sched_policy;
    struct sched_param schedp;
    sched_policy = SCHED_FIFO;
    schedp.sched_priority = 1;
    int ret = sched_setscheduler(0, sched_policy, &schedp);
    assert(ret==0);
    setpriority(PRIO_PROCESS, 0, -19);
}

/**
 * Initializes the UART to run at 115200 baud and
 * in RAW mode.
 * 
 * This function fails if it fails to set the device
 * attributes.
 */
void uart_init(int32_t fd)
{
    struct termios settings;
    assert(tcgetattr(fd, &settings)==0);
    cfsetospeed(&settings, B115200);
    cfmakeraw(&settings);
    assert(tcsetattr(fd, TCSANOW, &settings)==0);
}

int main()
{   

    /**
     * The purpose of this test is to see if I can
     * change the TX_ENABLE signal from HIGH to LOW
     * at the end of a data transmission.
     *  
     * If this is not possible withing 1 bit, then
     * the test is a failure.
     */

    configure_scheduler();
    wiringPiSetup();

    pinMode(4, OUTPUT);     // Setup TX_ENABLE pin as digital output
    digitalWrite(4, HIGH);  // Set TX_ENABLE HIGH

    int32_t fd = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
    assert(fd>=0);
    uart_init(fd);

    const char * msg = "foo";
    int32_t n = write(fd, msg, 3);
    const int32_t delay_us = 230;
    for (int32_t i = 0; i < delay_us; i++) delayMicroseconds(1);

    digitalWrite(4, LOW); // Set TX_ENABLE LOW
    close(fd);

    return 0;
}