#include <stdint.h>
#include <unistd.h>
#include <wiringPiI2C.h>
#include <stdio.h>

static int32_t voltage_adc_fd;
static int32_t current_adc_fd;

static const uint8_t voltage_adc_addr = 0b01010001;
static const uint8_t current_adc_addr = 0b01010000;

const float voltage_offset = 6.;
const float volts_per_int = 3.3/1024;
const float volts_per_amp = 0.2;

/**
 * Initializes I2C interface for voltage / current
 * monitoring
 * 
 * Returns 0 if successful.
 */
int32_t power_monitor_init(void)
{
    // get file descriptors for ADC devices
    uint32_t fd;
    fd = wiringPiI2CSetup(voltage_adc_addr);
    if (fd < 0) return -1;
    voltage_adc_fd = fd;

    fd = wiringPiI2CSetup(current_adc_addr);
    printf("current adc fd: %d\n", fd);
    if (fd < 0) return -1;
    current_adc_fd = fd;

    // write configuration bits to ADCs
    uint8_t reg_addr = 0x02;
    uint8_t config = 0b11100000;
    uint8_t result = wiringPiI2CWriteReg8(voltage_adc_fd, reg_addr, config);
    if (result < 0) return -1;
    result = wiringPiI2CWriteReg8(current_adc_fd, reg_addr, config);
    if (result < 0) return -1;

    // set register pointer
    wiringPiI2CWrite(voltage_adc_fd, 0x00);
    wiringPiI2CWrite(current_adc_fd, 0x00);
    return 0;
}


/**
 * Reads a 10-bit AD  conversion from the conversion register
 * @adc_fd      file descriptor of the adc
 * @return      returns the raw 10-bit conversion result
 */
uint16_t adc_read(int32_t adc_fd)
{
    uint16_t result = wiringPiI2CReadReg16(adc_fd, 0x00);
    if (result < 0) return result;
    uint16_t adc_read = (result & 0x000F) << 6;
    adc_read |= (result & 0xFC00) >> 10;
    return adc_read;
}

/**
 * Read voltage from battery voltage adc
 * 
 * @ptr    pointer to float that is overwritten by the read result
 * 
 * @return 0 if success, otherwise an error occurred
 */
int32_t read_voltage(float *ptr)
{
    uint16_t result = adc_read(voltage_adc_fd);
    if (result < 0) return result;
    *ptr = result * volts_per_int + voltage_offset;
    // *ptr = result * volts_per_int * amps_per_volt;
    return 0;
}

/**
 * Read current from battery current adc
 * 
 * @ptr:    pointer to float that is overwritten by the read result
 * 
 * @return  0 if success, otherwise an error occurred
 */
int32_t read_current(float *ptr)
{
    uint16_t result = adc_read(current_adc_fd);
    if (result < 0) return result;
    *ptr = result * volts_per_int / volts_per_amp;
    return 0;
}