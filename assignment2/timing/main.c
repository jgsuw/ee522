#include <assert.h>
#include <math.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

double mean(double *data, int count)
{
    assert(data!=NULL);
    assert(count>0);
    double sum =  0.;
    for (int i=0; i<count; i++) 
    {
        sum += data[i];
    }
    return sum/count;
}

double stddev(double *data, int count) 
{
    assert(data!=NULL);
    assert(count>0);
    double avg = mean(data, count);
    double sum = 0.;
    for (int i=0; i<count; i++)
    {
        sum += pow(data[i]-avg,2);
    }
    return pow(sum/(count-1), 0.5);
}

double ts_to_us(struct timespec tp)
{
    return 1e6*((double)tp.tv_sec) + ((double)tp.tv_nsec)/1e3;
}

bool alrm_caught = false;

void alrm_handler(int sigmsk)
{
    assert(sigmsk & SIGALRM);
    alrm_caught = true;
}

int main(int argc, char **argv)
{
    if (argc > 1)
    {
        int sched_policy;
        struct sched_param schedp;
        if (0==strcmp(argv[1], "SCHED_FIFO"))
        {
            sched_policy = SCHED_FIFO;
            schedp.sched_priority = 1;
        }
        if (0==strcmp(argv[1], "SCHED_RR"))
        {
            sched_policy = SCHED_RR;
            schedp.sched_priority = 1;
        }
        int ret = sched_setscheduler(0, sched_policy, &schedp);
        assert(ret==0);
    }

    setpriority(PRIO_PROCESS, 0, -1);

    int policy = sched_getscheduler(0);
    if (policy == SCHED_FIFO)
    {
        printf("Policy is real-time FIFO\n");
    }
    if (policy == SCHED_RR)
    {
        printf("Policy is real-time RR\n");
    }
    if (policy == SCHED_OTHER)
    {
        printf("Policy is RR\n", policy);
    }
    // set up signal handler for SIGALRM 
    struct sigaction sa;
    sa.sa_handler = (void*)alrm_handler;
    int ret = sigaction(SIGALRM, &sa, NULL);

    // configure signal timer
    int usec = 1000;
    struct itimerval itv;
    struct timeval it_interval = {0, usec};
    itv.it_interval = it_interval;
    itv.it_value = it_interval;
    ret = setitimer(ITIMER_REAL, &itv, NULL);
    assert(ret==0);

    // prepare main task    
    double t[1000];
    double last_time;
    int i = 0;
    struct timespec ts;
    while(true)
    {
        if (alrm_caught)
        {
            alrm_caught = false;
            ret = clock_gettime(CLOCK_REALTIME, &ts);
            assert(ret==0);
            t[i++] = ts_to_us(ts);
            if (i==1000)
            {
                i = 0;
                // suspend the timer while doing math
                struct timeval tv = {0,0};
                itv.it_value = tv;
                ret = setitimer(ITIMER_REAL, &itv, NULL);
                assert(ret==0);

                // do math
                double dt[999];
                double maxdt = 0.;
                for (int i=0; i<999; i++)
                {
                    dt[i] = t[i+1]-t[i];
                    if (dt[i] > maxdt) maxdt = dt[i];
                }
                double avg = mean(dt, 999);
                double dev = stddev(dt, 999);
                printf("avg (us): %08.4f \t stddev (us): %08.4f \t max (us): %08.4f\n"
                        , avg, dev, maxdt);

                // reenable timer
                itv.it_value = it_interval;
                ret = setitimer(ITIMER_REAL, &itv, NULL);
                assert(ret==0);
            }
        }
    }
}