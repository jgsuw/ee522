#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h> 
#include <time.h>
#include <sys/sem.h>
#include <errno.h>
int main() 
{ 
    const char * file_name = "FooBarBaz";
    const int proj_id = 42;

    // ftok to generate unique key 
    key_t key = ftok(file_name, proj_id); 
  
    // shmget returns an identifier in shmid 
    int shmid = shmget(key,1024,00666|IPC_CREAT); 
    
    if (shmid < 0) {
        printf("failed to get shared memory\n");
        printf("errno is %d\n", errno);
        return -1;
    }

    // get a semaphore from IPC V system
    int semid = semget(key, 1, 00666|IPC_CREAT);
    
    if (semid < 0) {
        printf("failed to get semaphore\n");
        printf("errno is %d\n", errno);
        return -1; 
    }

    // set the semaphore value to 1
    int semval = 1;
    int semnum = 0;
    int semctl_result = semctl(semid, semnum, SETVAL, semval);

    if (semctl_result < 0) {
        printf("failed to configure semaphore\n");
        printf("errno is %d\n", errno);
        shmctl(shmid, IPC_RMID, NULL);
        return -1;
    }

    // semaphore operations
    struct sembuf sem_lock = {0, -1, SEM_UNDO};
    struct sembuf sem_unlock = {0, 1, SEM_UNDO};
    size_t nops = 1;

    // shmat to attach to shared memory 
    char *str = (char*) shmat(shmid,(void*)0,0); 
    
    time_t start_time;
    time(&start_time);

    const int timeout = 20;

    time_t now;
    time(&now);

    int cnt = 0;
    while (now - start_time < timeout) {
        time(&now);
        // acquire the semaphore
        int result = semop(semid, &sem_lock, nops);
        if (0 == result) {
            sprintf(str, "%d", cnt++);
        }
        // release semaphore
        result = semop(semid, &sem_unlock, nops); 
    }
    printf("stopping program %d\n", now);

    //detach from shared memory  
    shmdt(str); 

    // destroy the semaphore
    semctl(semid, semnum, IPC_RMID);
   
    return 0; 
}
