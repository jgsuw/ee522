
#include <errno.h>
#include <sys/ipc.h> 
#include <sys/sem.h>
#include <sys/shm.h> 
#include <stdio.h> 
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#define STACK_SIZE 1024
#define FILE_NAME "ipc_test"
#define PROJ_ID 42

typedef struct {
    bool new_data;
} shared_mem_t;

struct sembuf sem_acquire[2] = {{0, 0, 0}, {0,1,SEM_UNDO}};
struct sembuf sem_release = {0, -1, SEM_UNDO};

bool reader_terminate = false;
bool writer_terminate = false;

double timespec_to_double(struct timespec tp)
{
    return ((double)tp.tv_sec) + ((double)tp.tv_nsec)/1e9;
} 

double timediff(struct timespec tp2, struct timespec tp1)
{
    double t1, t2;
    t1 = timespec_to_double(tp1);
    t2 = timespec_to_double(tp2);
    return t2-t1;
}

void writer(void *args)
{
    // Initialize shared memory and memory access semaphore.
    assert(args==NULL);
    key_t key = ftok(FILE_NAME, PROJ_ID);
    int shmid = shmget(key, sizeof(shared_mem_t), 00666|IPC_CREAT);
    assert(shmid>=0);
    int semid = semget(key, 1, 00666|IPC_CREAT);
    assert(semid>=0);
    int semval = 0;
    int semnum = 0;
    int semctl_result = semctl(semid, semnum, SETVAL, semval);
    assert(semctl_result >= 0);
    shared_mem_t *shared_mem = (shared_mem_t*)shmat(shmid, (void*)0, 0);

    // Initialize timing variables and data structures.
    clockid_t cpu_clk_id;
    int result = pthread_getcpuclockid((pthread_t)pthread_self(), &cpu_clk_id);
    assert(result==0);
    struct timespec t0, t1;

    // running loop
    double accumulator = 0.;
    int cnt = 0;
    do 
    {   
        result = semop(semid, sem_acquire, 2);
        if (!shared_mem->new_data)
        {
            clock_gettime(cpu_clk_id, &t0);
            shared_mem->new_data = true;
            clock_gettime(cpu_clk_id, &t1);
            accumulator += timediff(t1, t0);
            cnt+=1; 
        }
        result = semop(semid, &sem_release, 1);
        assert(result==0);
    }
    while(!writer_terminate);

    // detach from memory and destroy the semaphore
    shmdt(shared_mem);
    semctl(semid, semnum, IPC_RMID);

    printf("Average time to write memory %f\n", accumulator/cnt);
}

void reader(void *args)
{
    // Initialize shared memory and semaphore access
    assert(args==NULL);
    key_t key = ftok(FILE_NAME, PROJ_ID);
    int shmid = shmget(key, sizeof(shared_mem_t), 00666);
    assert(shmid>=0);
    int semid = semget(key, 1, 00666);
    assert(semid>=0);
    shared_mem_t *shared_mem = (shared_mem_t *)shmat(shmid, (void*)0, 0);

    // Initialize timing variables and data structures.
    clockid_t cpu_clk_id;
    int result = pthread_getcpuclockid(pthread_self(), &cpu_clk_id);
    assert(result==0);
    struct timespec new_time, old_time;
    double accumulator = 0.;
    int cnt = 0.;

    // running loop
    do 
    {
        if (result == 0)
        {
            result = semop(semid, sem_acquire, 2);
            assert(result==0);
            if (shared_mem->new_data) 
            {   
                clock_gettime(cpu_clk_id, &new_time);
                shared_mem->new_data = false;
                accumulator += timediff(new_time, old_time);
                cnt++;
                old_time = new_time;
            }
            result = semop(semid, &sem_release, 1);
            assert(result==0);
        }
    }
    while(!reader_terminate);

    // detach from memory and destroy the semaphore
    shmdt(shared_mem);
    printf("Average time between writes %f\n", accumulator/cnt);
} 

int main(void)
{
    // get a clock id for this process

    uint32_t reader_stack[STACK_SIZE/sizeof(uint32_t)];
    uint32_t writer_stack[STACK_SIZE/sizeof(uint32_t)];
    pthread_t reader_thread;
    pthread_t writer_thread;
    pthread_attr_t reader_attr;
    pthread_attr_init(&reader_attr);
    pthread_attr_t writer_attr;
    pthread_attr_init(&writer_attr);

    // create threads
    int ret;
    ret = pthread_attr_setstack(&reader_attr
                                , (void*)reader_stack
                                , STACK_SIZE); 
    ret = pthread_attr_setstack(&writer_attr
                                , (void*)writer_stack
                                , STACK_SIZE);
    ret = pthread_create(&writer_thread
                        , (const pthread_attr_t *) &writer_attr
                        , (void*)writer
                        , NULL);
    assert(ret==0);
    ret = pthread_create(&reader_thread
                        , (const pthread_attr_t *) &reader_attr
                        , (void*)reader
                        , NULL);
    assert(ret==0);

    double timeout = 360.;
    time_t start, now;
    time(&start);
    now = start;
    while (now-start < timeout)
    {
        time(&now);
    }
    reader_terminate = true;
    pthread_join(reader_thread, NULL);
    writer_terminate = true;
    pthread_join(writer_thread, NULL);
}