#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

int main()
{
    const char * file_name = "FooBarBaz";    
    const int proj_id = 65;

    // ftok to generate unique key 
    key_t key = ftok(file_name, proj_id); 
  
    // shmget returns an identifier in shmid 
    int shmid = shmget(key,1024,00666|IPC_CREAT); 
    
    if (shmid < 0) {
        printf("failed to get shared memory\n");
        printf("errno is %d\n", errno);
        return -1;
    }

    // get a semaphore from IPC V system
    int semid = semget(key, 1, 00666);
    
    if (semid < 0) {
        printf("failed to get semaphore\n");
        printf("errno is %d\n", errno);
        return -1; 
    }

    // semaphore operations
    struct sembuf sem_lock = {0, -1, SEM_UNDO};
    struct sembuf sem_unlock = {0, 1, SEM_UNDO};
    size_t nops = 1;

    // attach to shared memory
    char *str = (char *) shmat(shmid, (void*) 0, 0);

    // Loop control variables
    const int timeout = 20;
    time_t start_time; time(&start_time);
    time_t now = start_time;

    while (now - start_time < timeout) {
        time(&now);
        int result = semop(semid, &sem_lock, nops);
        if (0 == result) {
            printf("%s\n", str);
            semop(semid, &sem_unlock, nops);
        }
    }
    
    // detach from shared memory
    shmdt(str);
    
    // destroy the shared memory
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
