#include<assert.h>
#include<math.h>
#include<time.h>
#include<stdio.h>
#include<sys/time.h>
#include<sys/resource.h>
#include<pthread.h>

typedef enum {
PRIME,
COMPOSITE
} primality_t;

primality_t lucas_lehmer(int x)
{
    assert(x>0);
    int s = 4; 
    int M = ((int)pow(2.,(double)x))-1;
    assert(M>0);
    for (int i = 0; i < x-2; i++)
    {
        s = ((s*s)-2) % M;
    }
    primality_t result = s==0 ? PRIME : COMPOSITE;
    return result;
}

void stress_test(void)
{
    const int timeout = 20;
    time_t start, now;
    time(&start);
    time(&now);
    do 
    {  
        static int i = 1;
        primality_t result = lucas_lehmer(i%34);
        time(&now);
    }
    while (now-start < timeout);
}

int main(void)
{
    setpriority(PRIO_PROCESS, 0, -1);
    pthread_t threads[4];
    for (int i = 0; i < 4; i++) 
    {
        int ret = pthread_create(&threads[i]
                                , NULL
                                , (void*)stress_test
                                , NULL);
        assert(ret==0);
    }
    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);
    pthread_join(threads[2], NULL);
    pthread_join(threads[3], NULL);
}
